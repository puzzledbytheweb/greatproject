function handleFormSubmit() {
    const form = document.getElementById("signup");

    form.addEventListener("submit", event => {
        event.preventDefault();

        const name = form.children[0].value;
        const password = form.children[1].value;

        fetch("/user", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name,
                password
            })
        })
            .then(res => res.text())
            .then(text => console.log(text))
            .catch(err => console.error(err));
    });
}

handleFormSubmit();
