// Server side code!
const express = require("express");

// This technique is called object destructuring
// Here's a link -> https://medium.com/podiihq/destructuring-objects-in-javascript-4de5a3b0e4cb
const { users } = require("./database/database");

const { User } = require("./models/models");

const bodyparser = require("body-parser");

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(bodyparser.json());

app.use(express.static("public"));

app.listen(port, () => {
    console.log(`Server listening in port ${port}`);
});

app.get("/", (req, res) => {
    res.render("home", { route: "Pedro" });
});

app.get("/signup", (req, res) => {
    res.render("signup", { route: "Signup!!" });
});

app.get("/user", (req, res) => {
    res.send(users);
});

app.post("/user", (req, res) => {
    const { name, password } = req.body;

    const newUser = new User(name, password);

    users.push(newUser);

    res.send("Great!!");
});
