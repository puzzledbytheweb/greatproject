const User = require("./User/User");
const Dog = require("./Dog/Dog");

module.exports = {
    User,
    Dog
};
